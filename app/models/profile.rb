class Profile < ApplicationRecord
belongs_to :user
has_many :postingan
has_many :like, dependent: :destroy
has_many :likecomment, dependent: :destroy
has_many :comment, dependent: :destroy
has_many :Follower, class_name: "Follow", foreign_key: "followers_id", dependent: :destroy
has_many :Following, class_name: "Follow", foreign_key: "following_id", dependent: :destroy


  validates :profile_name, presence: true, length: { maximum: 15 }
  validates :user_id, presence: true
  validates :profile_bio, length: { maximum: 500 }



    def new_attributes
      {
        id: self.id,
        profile_name: self.profile_name,
        profile_bio: self.profile_bio,
        gender: self.gender,
        location: self.location,
        profil_pict: self.profil_pict,
        following_count: self.following_count,
        followers_count: self.followers_count,
        user_id: self.user_id,
        created_at: self.created_at   
     }
end
end
