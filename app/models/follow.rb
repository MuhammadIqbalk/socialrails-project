class Follow < ApplicationRecord
    belongs_to :follower, class_name: 'Profile', foreign_key: 'followers_id'
    belongs_to :following, class_name: 'Profile', foreign_key: 'following_id'

    validates :followers_id, presence: true
    validates :following_id, presence: true

    def new_attributes
        {
        id: self.id,
        followers_id: self.followers_id,
        following_id: self.following_id,
        created_at: self.created_at
     }
end
end
