class User < ApplicationRecord
    has_secure_password

    has_one :profile, dependent: :destroy

    validates :email, presence: true, length: { maximum: 255 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
                    uniqueness: true
    validates :username, presence: true, length: {maximum:15},
               uniqueness: true
    # validates :password_digest, presence: true
    

    def new_attributes
        {
           id: self.id,
           username: self.username,
           email: self.email,
           password_digest: self.password_digest,
           role: self.role,
           created_at: self.created_at,
           profile: self.profile
        }
        end
    end
