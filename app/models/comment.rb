class Comment < ApplicationRecord
    belongs_to :profile
    belongs_to :postingan
    has_many :likecomment, dependent: :destroy
    has_many :children, class_name: "Comment", foreign_key: "parent_id", dependent: :destroy
    belongs_to :parent, class_name: "Comment", foreign_key: "parent_id", optional: true

    
    validates :comment, presence: true, length: { maximum: 250 }
    validates :profile_id, presence: true
    validates :postingan_id, presence: true

    def new_attributes
        {
        id: self.id,
        comment: self.comment,
        profile_id: self.profile_id,
        parent: self.parent&.reply_comment_attributes,
        children: self.children&.map(&:reply_comment_attributes),
        postingan_id: self.postingan_id,
        profile_name: self.profile.profile_name,
        created_at: self.created_at
    }
end

def reply_comment_attributes
    {
      id: self.id,
      comment: self.comment,
      profile_name: self.profile.profile_name
    }
  end



end
