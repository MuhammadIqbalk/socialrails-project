class Postingan < ApplicationRecord
# belongs_to :profile, class_name: "Profile", foreign_key: "profile_id"
belongs_to :profile
belongs_to :room, class_name: "Room", foreign_key: "room_id"
has_many :comment, dependent: :destroy
has_many :like, dependent: :destroy


  validates :profile_id, presence: true
  validates :room_id, presence: true
  validates :caption, presence: true, length: { maximum: 500 }
  
  

    def new_attributes
      {
        id: self.id,
        caption: self.caption,
        post_img: self.post_img,
        like_count: self.like_count,
        comment_count: self.comment_count,
        profile_id: self.profile_id,
        room_id: self.room_id,
        created_at: self.created_at,
    }
end
end


