class Likecomment < ApplicationRecord
belongs_to :profile
belongs_to :comment

validates :comment_id, presence: true
validates :profile_id, presence: true

    def new_attributes
        {
        id: self.id,
        profile_id: self.profile_id,
        comment_id: self.comment_id,
    }
end
end
