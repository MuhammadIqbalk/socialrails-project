class Api::V1::CommentsController < ApplicationController
    # GET /comments
 def index
    @comments = Comment.all

    render json: @comments.map { |comment| comment.new_attributes }
  end

  # GET /comments/1
  def show
      @comments = Comment.find(params[:id])
    render json: @comments.new_attributes   
end

  # POST /comments
  def create
    @comments = Comment.create(comment_params)

    if @comments.save
      render json: @comments.new_attributes, status: :created
    else
      render json: @comments.errors, status: :unprocessable_entity
    end
  end

  # PUT /users/1
  def update
      @comments = Comment.find(params[:id])
    if @comments.update(comment_params)
      render json: @comments.new_attributes
    else
      render json: @comments.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comment/1
  def destroy
    @comments = Comment.find_by_id(params[:id])
    @comments.destroy
  end

  

  private
  def set_comment
    @comments = Comment.find_by_id(params[:id])
    if @comments.nil?
      render json: { error: "comment not found" }, status: :not_found
    end
  end

  def comment_params
      params.permit(
      :comment,
      :profile_id,
      :parent_id,
      :postingan_id
  )
  end
end
