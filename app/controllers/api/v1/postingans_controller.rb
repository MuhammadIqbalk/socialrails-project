class Api::V1::PostingansController < ApplicationController
    # GET /postingans
 def index
    @postingans = Postingan.all

    render json: @postingans.map { |postingan| postingan.new_attributes }
  end

  # GET /postingans/1
  def show
      @postingans = Postingan.find(params[:id])
    render json: @postingans.new_attributes   
end

  # POST /postingans
  def create
    @postingans = Postingan.create(postingan_params)

    if @postingans.save
      render json: @postingans.new_attributes, status: :created
    else
      render json: @postingans.errors, status: :unprocessable_entity
    end
  end

  # PUT /users/1
  def update
      @postingans = Postingan.find(params[:id])
    if @postingans.update(postingan_params)
      render json: @postingans.new_attributes
    else
      render json: @postingans.errors, status: :unprocessable_entity
    end
  end

  # DELETE /postingan/1
  def destroy
    @postingans = Postingan.find_by_id(params[:id])
    @postingans.destroy
  end

  

  private
  def set_postingan
    @postingans = Postingan.find_by_id(params[:id])
    if @postingans.nil?
      render json: { error: "postingan not found" }, status: :not_found
    end
  end

  def postingan_params
      params.permit(
      :caption,
      :post_img,
      :like_count,
      :comment_count,
      :profile_id,
      :room_id,
      
  )
  end

end

