class Api::V1::ProfilesController < ApplicationController
    # GET /profiles
    def index
        @profiles = Profile.all
    
        render json: @profiles.map { |profile| profile.new_attributes }
      end
    
      # GET /profiles/1
      def show
          @profiles = Profile.find(params[:id])
        render json: @profiles.new_attributes   
   end
    
      # POST /profiles
      def create
        @profiles = Profile.create(profile_params)
    
        if @profiles.save
          render json: @profiles.new_attributes, status: :created
        else
          render json: @profiles.errors, status: :unprocessable_entity
        end
      end
    
      # PUT /profiles/1
      def update
          @profiles = Profile.find(params[:id])
        if @profiles.update(profile_params)
          render json: @profiles.new_attributes
        else
          render json: @profiles.errors, status: :unprocessable_entity
        end
      end

      # DELETE /profile/1
      def destroy
        @profiles = Profile.find_by_id(params[:id])
        @profiles.destroy
      end
    
      
    
      private
      def set_profile
        @profiles = Profile.find_by_id(params[:id])
        if @profiles.nil?
          render json: { error: "Profile not found" }, status: :not_found
        end
      end
    
      def profile_params
          params.permit(
          :profile_name,
          :profile_bio,
          :gender,
          :location,
          :profile_pict,    
          :following_count,
          :followers_count,
          :user_id
      )
      end
end
