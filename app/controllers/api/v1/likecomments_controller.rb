class Api::V1::LikecommentsController < ApplicationController
    # GET /likecomments
 def index
    @likec = Likecomment.all

    render json: @likec.map { |likec| likec.new_attributes }
  end


  # POST /likecomments
  def create
    @likec = Likecomment.create(likec_params)

    if @likec.save
      render json: @likec.new_attributes, status: :created
    else
      render json: @likec.errors, status: :unprocessable_entity
    end
  end

  

  # DELETE /likecomment/1
  def destroy
    @likec = Likecomment.find_by_id(params[:id])
    @likec.destroy
  end

  

  private
  def set_like
    @likec = Likecomment.find_by_id(params[:id])
    if @likec.nil?
      render json: { error: "like not found" }, status: :not_found
    end
  end

  def likec_params
      params.permit(
      :profile_id,
      :comment_id
  )
  end

end
