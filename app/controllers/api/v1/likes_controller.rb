class Api::V1::LikesController < ApplicationController
 # GET /likes
 def index
    @likes = Like.all

    render json: @likes.map { |like| like.new_attributes }
  end


  # POST /likes
  def create
    @likes = Like.create(like_params)

    if @likes.save
      render json: @likes.new_attributes, status: :created
    else
      render json: @likes.errors, status: :unprocessable_entity
    end
  end

  

  # DELETE /like/1
  def destroy
    @likes = Like.find_by_id(params[:id])
    @likes.destroy
  end

  

  private
  def set_like
    @likes = Like.find_by_id(params[:id])
    if @likes.nil?
      render json: { error: "like not found" }, status: :not_found
    end
  end

  def like_params
      params.permit(
      :profile_id,
      :postingan_id
  )
  end

end
