class Api::V1::FollowsController < ApplicationController
 # GET /follows
 def index
    @follows = Follow.all

    render json: @follows.map { |follows| follows.new_attributes }
  end


  # POST /follows
  def create
    @follows = Follow.create(follows_params)

    if @follows.save
      render json: @follows.new_attributes, status: :created
    else
      render json: @follows.errors, status: :unprocessable_entity
    end
  end

  

  # DELETE /follows/1
  def destroy
    @follows = Follow.find_by_id(params[:id])
    @follows.destroy
  end

  

  private
  def set_like
    @follows = Follow.find_by_id(params[:id])
    if @follows.nil?
      render json: { error: "like not found" }, status: :not_found
    end
  end

  def follows_params
      params.permit(
      :followers_id,
      :following_id
  )
  end
end
