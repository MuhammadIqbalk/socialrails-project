class Api::V1::UsersController < ApplicationController
      before_action :authenticate_request!, exept: :login
    #  before_action :set_user, only: [:show, :update, :destroy]
  
    # GET /users
    def index
      @users = User.all
  
      render json: @users.map { |user| user.new_attributes }
    end
  
    # GET /users/1
    def show
        @users = User.find(params[:id])
      render json: @users.new_attributes
    end
  
    # POST /users
    def create
      @user = User.create(user_params)
  
      if @user.save
        render json: @user.new_attributes, status: :created
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  
    # PUT /users/1
    def update
        @user = User.find(params[:id])
      if @user.update(user_params)
        render json: @user.new_attributes
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /users/1
    def destroy
      @user = User.find_by_id(params[:id])
      # return render json: @user
      @user.destroy
    end
  
    def login
      @user = User.find_by(username: params[:username])
      if @user && @user&.authenticate(params[:password])
        token = JsonWebToken.encode(user_id: @user.id)
        render json: {
          user: @user.new_attributes,
          token: token,
        }
      else
        render json: { error: "Invalid username or password" }, status: :unauthorized
      end
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find_by_id(params[:id])
      if @user.nil?
        render json: { error: "User not found" }, status: :not_found
      end
    end
  
    def user_params
        params.permit(
        :username,
        :email,
        :password,
        :role
    )
    end
  
end
