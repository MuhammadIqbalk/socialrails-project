class Api::V1::RoomsController < ApplicationController
 # GET /rooms
 def index
    @rooms = Room.all

    render json: @rooms.map { |room| room.new_attributes }
  end

  # GET /rooms/1
  def show
      @rooms = Room.find(params[:id])
    render json: @rooms.new_attributes   
end

  # POST /rooms
  def create
    @rooms = Room.create(room_params)

    if @rooms.save
      render json: @rooms.new_attributes, status: :created
    else
      render json: @rooms.errors, status: :unprocessable_entity
    end
  end

  # PUT /rooms/1
  def update
      @rooms = Room.find(params[:id])
    if @rooms.update(room_params)
      render json: @rooms.new_attributes
    else
      render json: @rooms.errors, status: :unprocessable_entity
    end
  end

  # DELETE /room/1
  def destroy
    @rooms = Room.find_by_id(params[:id])
    @rooms.destroy
  end

  

  private
  def set_room
    @rooms = Room.find_by_id(params[:id])
    if @rooms.nil?
      render json: { error: "room not found" }, status: :not_found
    end
  end

  def room_params
      params.permit(
      :room_name,
      :room_desc
  )
  end

end
