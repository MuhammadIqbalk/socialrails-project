Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :profiles
      resources :postingans
      resources :comments
      resources :likecomments
      resources :follows
      resources :likes
      resources :rooms
      resources :users
      
      
       post '/login', to: 'authentication#authenticate_user'
    end
  end
end
  




