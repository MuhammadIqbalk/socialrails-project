class CreateLikecomments < ActiveRecord::Migration[7.0]
  def change
    create_table :likecomments do |t|
      t.integer :profile_id
      t.integer :comment_id
      t.timestamps
    end
  end
end
