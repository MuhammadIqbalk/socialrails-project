class CreateLikes < ActiveRecord::Migration[7.0]
  def change
    create_table :likes do |t|
      t.integer   :postingan_id
      t.integer   :profile_id
      t.timestamps
    end
  end
end
