class CreateFollows < ActiveRecord::Migration[7.0]
  def change
    create_table :follows do |t|
      t.integer :following_id
      t.integer :followers_id
      t.timestamps
    end
  end
end
