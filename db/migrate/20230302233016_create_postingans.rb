class CreatePostingans < ActiveRecord::Migration[7.0]
  def change
    create_table :postingans do |t|
      t.text :caption
      t.string :post_img
      t.integer :like_count
      t.integer :comment_count
      t.integer :profile_id
      t.integer :room_id

      t.timestamps
    end
  end
end
