class CreateComments < ActiveRecord::Migration[7.0]
  def change
    create_table :comments do |t|
      t.text :comment
      t.integer :profile_id
      t.integer :parent_id
      t.integer :postingan_id
      t.timestamps
    end
  end
end
