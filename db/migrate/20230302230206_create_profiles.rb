class CreateProfiles < ActiveRecord::Migration[7.0]
  def change
    create_table :profiles do |t|
      t.string :profile_name
      t.text :profile_bio
      t.integer :gender
      t.string :location
      t.string :profil_pict
      t.integer :following_count
      t.integer :followers_count
      t.integer :user_id
      t.timestamps
    end
  end
end
